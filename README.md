# README #

FortiOS unused objects finder

## Files ##

### unusedobjects.py ###

This will find unused address objects within a config file.

Run it from within the directory containing firewall config(s), then follow instructions on screen.

Without arguments it will display a list of duplicate objects in their respective VDOM's

usage: unusedobjects.py [-h] [-c]

optional arguments:

  -h, --help    show this help message and exit

  -c, --config  Print ready to copy/paste config to delete duplicate objects

  -n, --nodefaults  Do not count default objects: "SSLVPN_TUNNEL_ADDR1" and "all"

### README.md ###

This file

## Usage ##

unusedobjects.py [-h] [-c]

optional arguments:

  -h, --help    show this help message and exit

  -c, --config  Print ready to copy/paste config to delete duplicate objects

  -n, --nodefaults  Do not count default objects: "SSLVPN_TUNNEL_ADDR1" and "all"