#!/usr/bin/env python3

"""unusedobjects.py: FortiOS unused objects finder"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"

import os
import argparse

def printnumlines(lst, s=0):
    for n, item in enumerate(lst):
        print(str(n+s) + ' - ' + str(item))


def hasvdoms():
    ans = 0
    for _ in cfg:
        if _ == 'config vdom\n':
            ans = 1
            break
    return ans

BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal
print(chr(27) + "[2J")

print(GREEN + '\n\n\n\n\nWelcome!\nThis is FortiOS Unused Objects Finder v1.0\n' + RED + 'Run "unusedobjects.py -h" for help with options\n' + NORM)
# adding command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', help='Print ready to copy/paste config to delete unused objects', action='store_true')
parser.add_argument('-n', '--nodefaults', help='Do not count default objects: "SSLVPN_TUNNEL_ADDR1" and "all"', action='store_true')
args = parser.parse_args()

# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf to list "cfgfiles"
for file in dirs:
    if ".conf" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
else:
    print('\nOnly one config file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()
if args.config:
    print('\nSyntax to delete unused objects from ' + cfgfile + ':\n')
else:
    print('\nUnused objects in ' + cfgfile + ':\n')

# VDOMS
# slicing config file to start with 'config global\n' which will ease the vdom separation
confstartindex = 0
vdomstart = []
vdomend = []
vdlist = []
hasvd = hasvdoms()
if hasvd:
    for num, line in enumerate(cfg):
        if line == 'config global\n':
            confstartindex = num
    cfg = cfg[confstartindex:]

    for num, line in enumerate(cfg):
        if line == 'config vdom\n':
            vdomstart.append(num)

    for ind, startline in enumerate(vdomstart):
        for num, line in enumerate(cfg):
            if line == 'config vdom\n' and num > startline:
                vdomend.append(num)
                break
    vdomend.append(len(cfg))

    vdnum = range(len(vdomstart))
    obstart = []
    obend = []
    polstart = []
    polend = []
    grpstart = []
    grpend = []
    for vd in vdnum:
        checker = 0
        vdstart = vdomstart[vd]
        vdend = vdomend[vd]
        vdname = cfg[vdstart+1].strip(' edit').strip('\n')
        cfgslice = cfg[vdstart:vdend]
        vdlist.append(vdname)
        if args.config:
            print('config vdom\nedit ' + vdname + '\nconfig firewall address')
        else:
            print('\n\n#####   VDOM: ' + vdname + '   #####\n')

    # OBJECTS
        for num, line in enumerate(cfgslice):
            if line == 'config firewall address\n':
                obstart.append(num)
                checker = num
                break
        if checker == 0:
            obstart.append(0)
            obend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > obstart[vd]:
                    obend.append(num)
                    break
        obslice = cfgslice[obstart[vd]:obend[vd]]
        obtab = [line.strip(' edit ').strip('\n') for line in obslice if line.startswith('    edit ')]

    # POLICIES
        poltab = []
        checker = 0
        for num, line in enumerate(cfgslice):
            if line == 'config firewall policy\n':
                polstart.append(num)
                checker = num
                break
        if checker == 0:
            polstart.append(0)
            polend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > polstart[vd]:
                    polend.append(num)
                    break
        polslice = cfgslice[polstart[vd]:polend[vd]]
        poltab = [line.strip(' set srcaddr ').strip(' set dstaddr ').strip('\n') for line in polslice if line.startswith('        set srcaddr ') or line.startswith('        set dstaddr ')]

    # GROUPS
        grptab = []
        checker = 0
        for num, line in enumerate(cfgslice):
            if line == 'config firewall addrgrp\n':
                grpstart.append(num)
                checker = num
                break
        if checker == 0:
            grpstart.append(0)
            grpend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > grpstart[vd]:
                    grpend.append(num)
                    break
        grpslice = cfgslice[grpstart[vd]:grpend[vd]]
        grptab = [line.strip(' set member ').strip('\n') for line in grpslice if line.startswith('        set member ')]

    # OUTPUT
        unused = []
        for ob in obtab:
            checker = 0
            for pol in poltab:
                if ob in pol:
                    checker += 1
            for grp in grptab:
                if ob in grp:
                    checker += 1
            if checker == 0:
                unused.append(ob)
        if args.config:
            for line in unused:
                if args.nodefaults:
                    if line != '"SSLVPN_TUNNEL_ADDR1"' and line != '"all"':
                        print('delete ' + line)
                else:
                    print('delete ' + line)
            print('end\nend')
        else:
            if args.nodefaults:
                unused = [line for line in unused if line != '"SSLVPN_TUNNEL_ADDR1"' and line != '"all"']
            printnumlines(unused, 1)